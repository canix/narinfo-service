use crate::diesel::Connection;
use crate::diesel::RunQueryDsl;
use crate::models::NarinfoContent;
use diesel::PgConnection;

// Courtesy of https://snoozetime.github.io/2019/06/16/integration-test-diesel.html
// Keep the database info in mind to drop them later
pub struct TestContext {
    pub base_url: String,
    pub db_name: String,
}

/// Create a temporary database so we can run the tests in parallel.
/// The struct contains the name and the url of the data to establish later connections.
impl TestContext {
    pub fn new(base_url: &str, db_name: &str) -> Self {
        // First, connect to postgres db to be able to create our test
        // database.
        let postgres_url = format!("{}/postgres", base_url);
        let conn =
            PgConnection::establish(&postgres_url).expect("Cannot connect to postgres database.");

        // Create a new database for the test
        let query = diesel::sql_query(format!("CREATE DATABASE {}", db_name).as_str());
        query
            .execute(&conn)
            .expect(format!("Could not create database {}", db_name).as_str());

        Self {
            base_url: base_url.to_string(),
            db_name: db_name.to_string(),
        }
    }
}

/// When the TestContext is dropped, we destroy the associated database to prevent from side
/// effects.
impl Drop for TestContext {
    fn drop(&mut self) {
        let postgres_url = format!("{}/postgres", self.base_url);
        let conn =
            PgConnection::establish(&postgres_url).expect("Cannot connect to postgres database.");

        // Cant delete database if user are still connected.
        let disconnect_users = format!(
            "SELECT pg_terminate_backend(pid)
FROM pg_stat_activity
WHERE datname = '{}';",
            self.db_name
        );

        diesel::sql_query(disconnect_users.as_str())
            .execute(&conn)
            .unwrap();

        let query = diesel::sql_query(format!("DROP DATABASE {}", self.db_name).as_str());
        query
            .execute(&conn)
            .expect(&format!("Couldn't drop database {}", self.db_name));
    }
}

pub fn setup_database_and_get_connection(_context: &TestContext) -> PgConnection {
    let conn = PgConnection::establish(&format!("{}/{}", _context.base_url, _context.db_name))
        .expect(&format!("Cannot connect to {} database", _context.db_name));

    embed_migrations!("migrations/");
    let _ = embedded_migrations::run(&conn);
    // embedded_migrations::run_with_output(&conn, &mut std::io::stdout());

    conn
}

pub fn get_narinfo_content() -> NarinfoContent {
    NarinfoContent {
            store_path: String::from("/nix/store/inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24"),
            url: String::from(
                "nar/7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e.nar.xz",
            ),
            compression: Some(String::from("xz")),
            file_hash: String::from(
                "sha256:7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e",
            ),
            file_size: 1778300,
            nar_hash: String::from("sha256:09d79fsf628jvvnphmmp10nngmwanhgf5yk1dilsbv9n1mgqr8nf"),
            nar_size: 6303776,
            refs: String::from("33idnvrkvfgd5lsx2pwgwwi955adl6sk-glibc-2.31 4ljqqmxbh5dvh3wynd22vm3npxpxcd44-boost-1.69.0 5fz4mi6ghnq6qxy8y39m3sbpzwr6nzaw-perl-5.32.0 a3fc4zqaiak11jks9zd579mz5v0li8bg-bash-4.4-p23 azayfhqyg9mjyv6rv9bcypi6ws8aqfmy-gcc-wrapper-9.3.0 hxs99j1kx878pxxw5lbdarml69r5f1qb-gcc-9.3.0-lib inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24 v72cj06nk69cynckz2s12rhar25k1h7v-python3-3.8.5"),
            deriver: String::from("ayh06dd4fc8wh5j6jr8wqwp02r16gnmp-simgrid-3.24.drv"),
            sig: Some(String::from("nur-kapack.cachix.org-1:ZOkcFyWU38dK8Qd4i3Ol7zZVtMCW19+oJynGQMhiMJSgRn4AbH3QwCIHChH2saWNiz7g0oyyinMBjN/YuEgmBQ=="))}
}
