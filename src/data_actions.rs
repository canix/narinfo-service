use crate::models::{Cache, Narinfo};
use diesel::insert_into;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};

// Create the connections manager pool
pub fn connect_db(database_url: &str) -> Pool<ConnectionManager<PgConnection>> {
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    Pool::new(manager).expect("Failed to create pool.")
}

/// Find the given narinfo file.
/// The attribute `narinfo_filename` must be formatted as `<sha256>.narinfo` (this format is not
/// checked, any request not properly formated will lead to a 404 not found.).
/// The `cache_name` must be the name of an existing cache name.
/// Returning errors related to `Diesel`.
/// The Ok return type is a tuple so one can identify if
/// It is the cache_name that doesn't exist or the narinfo file.
pub fn find_narinfo(
    cache_name: String,
    narinfo_filename: String,
    connection: &PgConnection,
) -> Result<(Option<Cache>, Option<Narinfo>), diesel::result::Error> {
    use crate::schema::{caches, narinfos};

    match caches::table
        .find(cache_name.clone())
        .get_result::<Cache>(&*connection)
        .optional()?
    {
        Some(ref cache) => Ok((
            Some(cache.clone()),
            Narinfo::belonging_to(cache)
                .find((narinfo_filename, cache_name))
                .select((
                    narinfos::narinfo_hash,
                    narinfos::cache_name,
                    (
                        narinfos::store_path,
                        narinfos::url,
                        narinfos::compression,
                        narinfos::file_hash,
                        narinfos::file_size,
                        narinfos::nar_hash,
                        narinfos::nar_size,
                        narinfos::refs,
                        narinfos::deriver,
                        narinfos::sig,
                    ),
                ))
                .get_result::<Narinfo>(&*connection)
                .optional()?,
        )),
        None => return Ok((None, None)),
    }
}

/// Find the cache with its id.
pub fn find_cache(
    cache_name: String,
    connection: &PgConnection,
) -> Result<Option<Cache>, diesel::result::Error> {
    use crate::schema::caches::dsl::*;

    caches
        .find(cache_name.clone())
        .get_result::<Cache>(&*connection)
        .optional()
}

/// Find the cache.
pub fn create_cache(
    cache: Cache,
    connection: &PgConnection,
) -> Result<Cache, diesel::result::Error> {
    use crate::schema::caches;

    insert_into(caches::table)
        .values(&cache)
        .get_result(&*connection)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utilities::{
        get_narinfo_content, setup_database_and_get_connection, TestContext,
    };
    use diesel::result::Error;
    use std::env;

    #[test]
    fn test_find_narinfo() {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let _context = TestContext::new(&database_url, "data_actions_test_find_narinfo");

        let connection = connection_with_data_tests(&_context);

        connection.test_transaction::<_, Error, _>(|| {
            let narinfo = find_narinfo(
                String::from("unknow"),
                String::from("unknow.narinfo"),
                &connection,
            )
            .unwrap();
            assert_eq!(narinfo, (None, None));
            Ok(())
        });
    }

    #[test]
    fn test_find_cache() {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let _context = TestContext::new(&database_url, "data_actions_test_find_narinfo_cache");
        let connection = connection_with_data_tests(&_context);

        connection.test_transaction::<_, Error, _>(|| {
            let unknow_cache = find_cache(String::from("unknow"), &connection).unwrap();
            assert_eq!(unknow_cache, None);

            let test_cache = find_cache(String::from("cache_test1"), &connection).unwrap();
            assert_eq!(
                test_cache,
                Some(Cache {
                    name: String::from("cache_test1")
                })
            );
            Ok(())
        });
    }

    #[test]
    fn test_create_cache() {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let _context = TestContext::new(&database_url, "data_actions_test_create_cache");
        let connection = connection_with_data_tests(&_context);

        use diesel::result::DatabaseErrorKind;
        use diesel::result::Error::DatabaseError;

        connection.test_transaction::<_, Error, _>(|| {
            // First we try the insertion with a cache that should not exist (this must succeed)
            let instertable_cache = create_cache(
                Cache {
                    name: String::from("unknow"),
                },
                &connection,
            )
            .unwrap();
            assert_eq!(
                Cache {
                    name: String::from("unknow")
                },
                instertable_cache
            );

            // Cache name already into the db (should fail)
            let collide_cache = create_cache(
                Cache {
                    name: String::from("cache_test1"),
                },
                &connection,
            );

            assert_eq!(
                DatabaseError(
                    DatabaseErrorKind::UniqueViolation,
                    Box::new(String::from(
                        "duplicate key value violates unique constraint \"caches_pkey\""
                    ))
                ),
                collide_cache.unwrap_err()
            );
            Ok(())
        });
    }

    fn connection_with_data_tests(_context: &TestContext) -> PgConnection {
        use crate::schema::*;

        let conn = setup_database_and_get_connection(_context);

        let cache_test_data = vec![
            Cache {
                name: String::from("cache_test1"),
            },
            Cache {
                name: String::from("cache_test2"),
            },
        ];

        // Insert data for tests
        insert_into(caches::table)
            .values(&cache_test_data)
            .execute(&conn)
            .unwrap();

        let narinfo_test_data = vec![Narinfo {
            narinfo_hash: String::from("inh6kxgb89byi8pf2j0v5jyi9p26mfh5"),
            cache_name: String::from("cache_test1"),
            content: get_narinfo_content(),
        }];

        insert_into(narinfos::table)
            .values(&narinfo_test_data)
            .execute(&conn)
            .unwrap();

        // Return the connection for the tests
        conn
    }
}
