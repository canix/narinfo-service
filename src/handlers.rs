use crate::models::Cache;
use actix_web::{error::BlockingError, http::Method, post, route, web, HttpRequest, HttpResponse};
use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::result::{DatabaseErrorKind, Error::DatabaseError};
use log::{trace, warn};
use serde::Deserialize;

#[route(
    "/caches/{cache_name}/{narinfo_hash}.narinfo",
    method = "GET",
    method = "HEAD"
)]
pub async fn find_narinfo(
    web::Path((cache_name, narinfo_hash)): web::Path<(String, String)>,
    _req: HttpRequest,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> HttpResponse {
    trace!("[{}] {}/{}", _req.method(), cache_name, narinfo_hash);
    let conn = pool.get().expect("couldn't get db connection from pool");

    let req_res =
        web::block(move || crate::data_actions::find_narinfo(cache_name, narinfo_hash, &*conn))
            .await;

    match req_res {
        Ok((_, Some(narinfo))) => {
            let mut ok = HttpResponse::Ok();
            ok.content_type("text/x-nix-narinfo");

            // Type returned by cachix and cache.nixos.org
            // try `curl -I https://cache.nixos.org/3jjqmr9r9ydqr4yvzs2vl43115v5z0j7.narinfo`
            match _req.method() {
                // In case of a HEAD method, we do not return a body
                &Method::HEAD => ok.finish(),
                &Method::GET => ok.body(narinfo.content.to_string()),
                method => panic!("Not supported method: {}", method),
            }
        }
        Ok((_, None)) => HttpResponse::NotFound().finish(),
        Err(e) => {
            warn!("Unhandled error: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[post("/caches/{cache_name}/{narinfo_hash}.narinfo")]
pub async fn post_narinfo(
    web::Path((cache_name, narinfo_hash)): web::Path<(String, String)>,
    req_body: String,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> actix_web::Result<String> {
    Ok(format!(
        "Narfile registred: {},{}, {}",
        cache_name, narinfo_hash, req_body
    ))
}

#[derive(Debug, Deserialize, Clone)]
pub struct CreateCacheParams {
    name: String,
}

#[post("/caches")]
pub async fn create_cache(
    create_cache_params: web::Query<CreateCacheParams>,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> HttpResponse {
    trace!(
        "Received put request with cache name: {:?}",
        create_cache_params
    );

    let conn = pool.get().expect("couldn't get db connection from pool");

    let req_res = web::block(move || {
        crate::data_actions::create_cache(
            Cache {
                name: create_cache_params.name.clone(),
            },
            &*conn,
        )
    })
    .await;

    //TODO: Find a way to make this error handling hell better
    // The first solution that comes to my mind is to create a dedicated error type into the
    // `data_action` module with a semantic related to my project. Instead of returning raw diesel
    // errors.
    match req_res {
        Ok(cache) => HttpResponse::Created().body(format!("caches/{}", cache.name)),
        Err(database_error) => match database_error {
            BlockingError::Error(DatabaseError(DatabaseErrorKind::UniqueViolation, _)) => {
                HttpResponse::Conflict().body(format!("Resource already exists"))
            }
            _ => {
                warn!("Unhandled error type: {}", database_error);
                HttpResponse::InternalServerError().finish()
            }
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::diesel::RunQueryDsl;
    use crate::models::*;
    use crate::test_utilities::TestContext;
    use actix_http::{Error, Request};
    use actix_web::body::MessageBody;
    use actix_web::dev::{HttpServiceFactory, Service, ServiceResponse};
    use actix_web::{http::StatusCode, test, test::TestRequest, web::Bytes, App};
    use std::env;

    #[actix_rt::test]
    async fn test_hanler_create_cache() {
        let _context = TestContext::new(
            &env::var("DATABASE_URL").expect("DATABASE_URL must be set"),
            "test_handler_create_cache",
        );
        let mut app = get_app(&_context, create_cache).await;

        let resp = test::call_service(
            &mut app,
            TestRequest::post()
                .uri("/caches?name=should_work")
                .to_request(),
        )
        .await;
        // If the cache is succesfully created, the server returns a CREATED http code
        assert_eq!(resp.status(), StatusCode::CREATED);
        // The body of the request contains the path to the resource
        let result = test::read_body(resp).await;
        assert_eq!(Bytes::from_static(b"caches/should_work"), result);

        // This request should fail as the cache already exists
        let already_existing = test::call_service(
            &mut app,
            TestRequest::post()
                .uri("/caches?name=cache_test1")
                .to_request(),
        )
        .await;
        assert_eq!(already_existing.status(), StatusCode::CONFLICT);
    }

    #[actix_rt::test]
    async fn test_hanler_find_narinfo() {
        let _context = TestContext::new(
            &env::var("DATABASE_URL").expect("DATABASE_URL must be set"),
            "test_hanler_find_narinfo",
        );
        let mut app = get_app(&_context, find_narinfo).await;

        // First test with existing data, should return OK (200)
        let resp = test::call_service(
            &mut app,
            TestRequest::default()
                .uri("/caches/cache_test2/inh6kxgb89byi8pf2j0v5jyi9p26mfh5.narinfo")
                .to_request(),
        )
        .await;
        assert_eq!(resp.status(), StatusCode::OK);

        // First test with nonexistent data, should return NOT_FOUND (404)
        let not_found = test::call_service(
            &mut app,
            TestRequest::default()
                .uri("/caches/cache_test1/unknow.narinfo")
                .to_request(),
        )
        .await;
        assert_eq!(not_found.status(), StatusCode::NOT_FOUND);
    }

    /// Function that return a configured app with the tested service
    /// Largely inspired from https://github.com/actix/actix-web/issues/1147
    // For the moment this function is a bit overkill, however for later tests with more than one
    // services, config and middlewares it should be avoid us to duplicate too much code.
    pub async fn get_app<F>(
        context: &TestContext,
        service: F,
    ) -> impl Service<Request = Request, Response = ServiceResponse<impl MessageBody>, Error = Error>
    where
        F: HttpServiceFactory + 'static,
    {
        let pool = setup_database_and_get_pool(context);
        test::init_service(App::new().data(pool.clone()).service(service)).await
    }

    /// Create a connection pool according to a `TestContext`, run migrations and feed some data
    /// for later tests.
    pub fn setup_database_and_get_pool(
        _context: &TestContext,
    ) -> Pool<ConnectionManager<PgConnection>> {
        let pool: Pool<ConnectionManager<PgConnection>> = crate::data_actions::connect_db(
            &*format!("{}/{}", _context.base_url, _context.db_name),
        );

        let conn = pool.get().expect("couldn't get db connection from pool");

        embed_migrations!("migrations/");
        let _ = embedded_migrations::run(&conn);

        setup_data_test(&conn);

        pool
    }

    /// Feed the database with data samples
    fn setup_data_test(conn: &PgConnection) {
        use crate::schema::*;
        use diesel::insert_into;

        let cache_test_data = vec![
            Cache {
                name: String::from("cache_test1"),
            },
            Cache {
                name: String::from("cache_test2"),
            },
        ];

        // Insert data for tests
        insert_into(caches::table)
            .values(&cache_test_data)
            .execute(conn)
            .unwrap();

        let narinfo_test_data = vec![Narinfo {
            narinfo_hash: String::from("inh6kxgb89byi8pf2j0v5jyi9p26mfh5"),
            cache_name: String::from(
                "cache_test2",
            ),
            content: NarinfoContent {
            store_path: String::from("/nix/store/inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24"),
            url: String::from(
                "nar/7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e.nar.x",
            ),
            compression: Some(String::from("xz")),
            file_hash: String::from(
                "sha256:7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e",
            ),
            file_size: 1778300,

            nar_hash: String::from("sha256:09d79fsf628jvvnphmmp10nngmwanhgf5yk1dilsbv9n1mgqr8nf"),
            nar_size: 6303776,
            refs: String::from("33idnvrkvfgd5lsx2pwgwwi955adl6sk-glibc-2.31 4ljqqmxbh5dvh3wynd22vm3npxpxcd44-boost-1.69.0 5fz4mi6ghnq6qxy8y39m3sbpzwr6nzaw-perl-5.32.0 a3fc4zqaiak11jks9zd579mz5v0li8bg-bash-4.4-p23 azayfhqyg9mjyv6rv9bcypi6ws8aqfmy-gcc-wrapper-9.3.0 hxs99j1kx878pxxw5lbdarml69r5f1qb-gcc-9.3.0-lib inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24 v72cj06nk69cynckz2s12rhar25k1h7v-python3-3.8.5"),
            deriver: String::from("ayh06dd4fc8wh5j6jr8wqwp02r16gnmp-simgrid-3.24.drv"),
            sig: Some(String::from("nur-kapack.cachix.org-1:ZOkcFyWU38dK8Qd4i3Ol7zZVtMCW19+oJynGQMhiMJSgRn4AbH3QwCIHChH2saWNiz7g0oyyinMBjN/YuEgmBQ==")),
        }}];

        insert_into(narinfos::table)
            .values(&narinfo_test_data)
            .execute(conn)
            .unwrap();
    }
}
