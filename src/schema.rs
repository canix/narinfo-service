table! {
    caches (name) {
        name -> Varchar,
    }
}

table! {
    narinfos (narinfo_hash, cache_name) {
        narinfo_hash -> Varchar,
        cache_name -> Varchar,
        store_path -> Varchar,
        url -> Varchar,
        compression -> Nullable<Varchar>,
        file_hash -> Varchar,
        file_size -> Int8,
        nar_hash -> Varchar,
        nar_size -> Int8,
        refs -> Varchar,
        deriver -> Varchar,
        sig -> Nullable<Varchar>,
    }
}

joinable!(narinfos -> caches (cache_name));

allow_tables_to_appear_in_same_query!(
    caches,
    narinfos,
);
