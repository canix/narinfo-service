use crate::schema::caches;
use crate::schema::narinfos;
use std::fmt;
use std::str::FromStr;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::IResult;

#[derive(Identifiable, Queryable, Insertable, PartialEq, Debug, Clone)]
#[table_name = "caches"]
#[primary_key(name)]
pub struct Cache {
    pub name: String,
}

/// Base struct for the narinfo files available into a binary cache.
/// This struct is split into two parts.
/// The attributes `narinfo_hash` and `cache_name` represent the contextual informations of the
/// narinfo.
/// The `content` part should represent the content of a narinfo that is served.
#[derive(Identifiable, Insertable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Cache, foreign_key = "cache_name")]
#[primary_key(narinfo_hash, cache_name)]
#[table_name = "narinfos"]
pub struct Narinfo {
    pub narinfo_hash: String,
    pub cache_name: String,
    #[diesel(embed)]
    pub content: NarinfoContent,
}

#[derive(Queryable, Insertable, PartialEq, Debug, Builder)]
#[table_name = "narinfos"]
pub struct NarinfoContent {
    pub store_path: String,
    pub url: String,
    pub compression: Option<String>,
    pub file_hash: String,
    pub file_size: i64,
    pub nar_hash: String,
    pub nar_size: i64,
    pub refs: String,
    pub deriver: String,
    //TODO: Handle multiple signature
    pub sig: Option<String>,
}

/// Recognize the different keys composing a narinfo using the nom crate.
fn narinfo_keys_parser(i: &str) -> IResult<&str, &str> {
    alt((
        tag("StorePath:"),
        tag("URL:"),
        tag("Compression:"),
        tag("FileHash:"),
        tag("FileSize:"),
        tag("NarHash:"),
        tag("NarSize:"),
        tag("References:"),
        tag("Deriver:"),
        tag("Sig:"),
    ))(i)
}

impl FromStr for NarinfoContent {
    //TODO: Proper error management
    type Err = std::string::String;
    fn from_str(narinfo_file: &str) -> Result<Self, Self::Err> {
        let mut builder = NarinfoContentBuilder::default();
        for line in narinfo_file.lines() {
            let (value, key) = match narinfo_keys_parser(line) {
                Ok(res) => res,
                // We may also ignore it and continue the parsing ?
                Err(err) => return Err(format!("{}", err)),
            };
            // Narinfo content is formated as "key: value", with a space before value,
            // however idk yet if this space is mandatory. If it is not mandatory, It is not
            // possible to include it into the parsing (as a tag) so I trim it here on the fly.
            let trimed_value = value.trim();
            match key {
                "StorePath:" => builder.store_path(String::from(trimed_value)),
                "URL:" => builder.url(String::from(trimed_value)),
                "Compression:" => builder.compression(Some(String::from(trimed_value))),
                "FileHash:" => builder.file_hash(String::from(trimed_value)),
                "FileSize:" => {
                    builder.file_size(trimed_value.parse::<i64>().expect("Cannot parse FileSize"))
                }
                "NarHash:" => builder.nar_hash(String::from(trimed_value)),
                "NarSize:" => {
                    builder.nar_size(trimed_value.parse::<i64>().expect("Cannot parse NasSize"))
                }
                "References:" => builder.refs(String::from(trimed_value)),
                "Deriver:" => builder.deriver(String::from(trimed_value)),
                "Sig:" => builder.sig(Some(String::from(trimed_value))),
                _ => &mut builder,
            };
        }
        builder.build()
    }
}

impl fmt::Display for NarinfoContent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "StorePath: {}\n", self.store_path).expect("Could not write StorPath");
        write!(f, "URL: {}\n", self.url).expect("Could not write URL");
        match &self.compression {
            Some(comp) => {
                write!(f, "Compression: {}\n", comp).expect("Could no write Compression");
            }
            None => {}
        };
        write!(f, "FileHash: {}\n", self.file_hash).expect("Could not write FileHash");
        write!(f, "FileSize: {}\n", self.file_size).expect("Could not write FileSize");
        write!(f, "NarHash: {}\n", self.nar_hash).expect("Could not write NarHash");
        write!(f, "NarSize: {}\n", self.nar_size).expect("Could not write NarSize");
        write!(f, "References: {}\n", self.refs).expect("Could not write References");
        match &self.sig {
            Some(signature) => {
                write!(f, "Deriver: {}\n", self.deriver).expect("Could not write Deriver");
                write!(f, "Sig: {}\n", signature)
            }
            None => write!(f, "Deriver: {}\n", self.deriver),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utilities::get_narinfo_content;

    #[test]
    fn test_display_narinfo() {
        let narinfo_content = get_narinfo_content();
        assert_eq!(
            format!("{}", narinfo_content),
            r#"StorePath: /nix/store/inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24
URL: nar/7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e.nar.xz
Compression: xz
FileHash: sha256:7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e
FileSize: 1778300
NarHash: sha256:09d79fsf628jvvnphmmp10nngmwanhgf5yk1dilsbv9n1mgqr8nf
NarSize: 6303776
References: 33idnvrkvfgd5lsx2pwgwwi955adl6sk-glibc-2.31 4ljqqmxbh5dvh3wynd22vm3npxpxcd44-boost-1.69.0 5fz4mi6ghnq6qxy8y39m3sbpzwr6nzaw-perl-5.32.0 a3fc4zqaiak11jks9zd579mz5v0li8bg-bash-4.4-p23 azayfhqyg9mjyv6rv9bcypi6ws8aqfmy-gcc-wrapper-9.3.0 hxs99j1kx878pxxw5lbdarml69r5f1qb-gcc-9.3.0-lib inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24 v72cj06nk69cynckz2s12rhar25k1h7v-python3-3.8.5
Deriver: ayh06dd4fc8wh5j6jr8wqwp02r16gnmp-simgrid-3.24.drv
Sig: nur-kapack.cachix.org-1:ZOkcFyWU38dK8Qd4i3Ol7zZVtMCW19+oJynGQMhiMJSgRn4AbH3QwCIHChH2saWNiz7g0oyyinMBjN/YuEgmBQ==
"#
        );
    }

    #[test]
    fn test_from_str_narinfo() {
        // Testing valid narinfo file
        let narinfo_content = r#"StorePath: /nix/store/inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24
URL: nar/7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e.nar.xz
Compression: xz
FileHash: sha256:7a887bba4e34507c0842845d8864c58e92f44f4a3046e430047302cc5a693a5e
FileSize: 1778300
NarHash: sha256:09d79fsf628jvvnphmmp10nngmwanhgf5yk1dilsbv9n1mgqr8nf
NarSize: 6303776
References: 33idnvrkvfgd5lsx2pwgwwi955adl6sk-glibc-2.31 4ljqqmxbh5dvh3wynd22vm3npxpxcd44-boost-1.69.0 5fz4mi6ghnq6qxy8y39m3sbpzwr6nzaw-perl-5.32.0 a3fc4zqaiak11jks9zd579mz5v0li8bg-bash-4.4-p23 azayfhqyg9mjyv6rv9bcypi6ws8aqfmy-gcc-wrapper-9.3.0 hxs99j1kx878pxxw5lbdarml69r5f1qb-gcc-9.3.0-lib inh6kxgb89byi8pf2j0v5jyi9p26mfh5-simgrid-3.24 v72cj06nk69cynckz2s12rhar25k1h7v-python3-3.8.5
Deriver: ayh06dd4fc8wh5j6jr8wqwp02r16gnmp-simgrid-3.24.drv
Sig: nur-kapack.cachix.org-1:ZOkcFyWU38dK8Qd4i3Ol7zZVtMCW19+oJynGQMhiMJSgRn4AbH3QwCIHChH2saWNiz7g0oyyinMBjN/YuEgmBQ==
"#;
        assert_eq!(
            Ok(get_narinfo_content()),
            NarinfoContent::from_str(narinfo_content)
        );
    }
}
