use actix_web::{App, HttpServer};

#[macro_use]
extern crate derive_builder;
extern crate nom;

#[macro_use]
extern crate diesel;
use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};

#[macro_use]
extern crate diesel_migrations;

#[cfg(feature = "test-utilities")]
mod test_utilities;

mod data_actions;
mod handlers;
mod models;
mod schema;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    let pool: Pool<ConnectionManager<PgConnection>> =
        crate::data_actions::connect_db("postgres://localhost:5432/postgres");
    //TODO: Make it configurable
    let bind = "127.0.0.1:8080";

    let server = HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .service(crate::handlers::find_narinfo)
            .service(crate::handlers::create_cache)
            .service(crate::handlers::post_narinfo)
    })
    .bind(&bind)?
    .run();
    println!("Server running at http://{}/", &bind);
    server.await
}
