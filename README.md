# Diesel

To install the diesel_cli with cargo:

```
$ cargo install diesel_cli --no-default-features --features postgres
```

run migration with diesel
```
$ diesel migration run --database-url postgres://localhost:5432/postgres
```

Some indication about unit testing diesel code:
https://github.com/diesel-rs/diesel/issues/1549

Run the tests:
```
DATABASE_URL="postgres://127.0.0.1" cargo test --features=test-utilities -- --nocapture
```

Run the command with a shell
Before starting the tests, one must start the database,

```
pg_ctl start -D /data/postgres/
```

```
nix-shell -A devShell.x86_64-linux  --command  "DATABASE_URL="postgres://127.0.0.1" cargo test --features=test-utilities -- --nocapture"
```

# CI

Build docker for ci:

```
docker build -t adfaure/canix-ci:latest ci
```

Inside the ci docker, once the database is running:

```
nix-shell -A devShell.x86_64 --command "DATABASE_URL='postgres://postgres@127.0.0.1' cargo test --features=test-utilities"
```

The postgres conf must contain the following lines:

```
listen_addresses = '127.0.0.1'		# what IP address(es) to listen on;
unix_socket_directories = '/tmp/postgres'	# comma-separated list of directories
```

These commands should run the tests:

```
mkdir -p /tmp/postgres && chown -R postgres /tmp/postgres
nix-shell -A devShell --command 'su postgres -c "$(which pg_ctl) init -D /tmp/postgres"'
echo "listen_addresses = '127.0.0.1'" >> /tmp/postgres/postgresql.conf
echo "unix_socket_directories = '/tmp/postgres'" >> /tmp/postgres/postgresql.conf
nix-shell -A devShell --command 'su postgres -c "$(which pg_ctl) -D /tmp/postgres -l /tmp/logfile start"'
nix-shell -A devShell --command "DATABASE_URL='postgres://postgres@127.0.0.1' cargo test --features=test-utilities"
```


