# For legacy call
# Transforms a flake project into a "standard" `default.nix`
# Now, one can call the project without flake activated with `nix-shell -A devShell`
(import (fetchTarball {
  url =
    "https://github.com/edolstra/flake-compat/archive/99f1c2157fba4bfe6211a321fd0ee43199025dbf.tar.gz";
  sha256 = "sha256:0x2jn3vrawwv9xp15674wjz9pixwjyj3j771izayl962zziivbx2";
}) { src = ./.; }).defaultNix
