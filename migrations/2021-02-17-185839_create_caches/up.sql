-- Your SQL goes here

CREATE TABLE caches (
    name          varchar(50) PRIMARY KEY
);

CREATE TABLE narinfos (
    narinfo_hash  varchar(85),
    cache_name    varchar(50) NOT NULL,
    store_path    varchar(75) NOT NULL,
    url           varchar(2000) NOT NULL,
    compression   varchar(15),
    file_hash     varchar(85) NOT NULL,
    file_size     bigint NOT NULL,
    nar_hash      varchar(85) NOT NULL,
    nar_size      bigint NOT NULL,
    -- reference  s is a keyword in SQL, therefore I cannot use it as table attribute name
    refs          varchar(1000) NOT NULL,
    deriver       varchar(85) NOT NULL,
    -- How can w  e handle multiple signatures ?
    sig           varchar(220),
    PRIMARY KEY(narinfo_hash, cache_name),
    CONSTRAINT fk_customer
      FOREIGN KEY(cache_name)
        REFERENCES caches(name)
);
